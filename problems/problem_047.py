# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
        characters = 0
        special_characters = 0
        digits = 0
        uppercase = 0
        lowercase = 0
        for char in password:
            if char.isalpha():
                characters += 1
                if char.isupper():
                    uppercase += 1
                elif char.islower():
                    lowercase += 1
            elif char.isdigit():
                digits += 1
            else:
                special_characters += 1
        if (
        lowercase >= 1 and
        uppercase >= 1 and
        digits >= 1 and
        special_characters >= 1 and 
        len(password) >= 6 and 
        len(password) <= 12):
            return True
        else:
            return False
         
